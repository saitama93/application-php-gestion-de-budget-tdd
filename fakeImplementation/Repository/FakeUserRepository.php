<?php

namespace App\FakeImplementation\Repository;

use App\Domain\User\Model\User;
use App\Domain\User\Repository\UserRepositoryInterface;
use Ramsey\Uuid\Uuid;

final class FakeUserRepository implements UserRepositoryInterface
{
    const GOOD_USERNAME = 'user1';

    private array $users;

    public function __construct()
    {
        $this->users = ['user1' => [
                'id' => Uuid::uuid4()->toString(),
                'username' => 'user1',
                'firstname' => 'John',
                'lastname' => 'John',
                'password' => 'password',
            ],
            'user2' => [
                'id' => Uuid::uuid4()->toString(),
                'username' => 'user2',
                'firstname' => 'John2',
                'lastname' => 'John2',
                'password' => 'password',
            ]
        ];
    }

    public function save(User $user): void
    {
        $this->users[$user->getUsername()] = $user;
    }

    public function checkIfUsernameAlreadyExists(string $username): bool
    {
        return array_key_exists($username, $this->users);
    }

    public function findByUsername(string $username): ?array
    {
        $user = $this->getUsers()[$username] ?? null;

        if ($user) {
            $user['password'] = password_hash(
                $user['password'],
                PASSWORD_BCRYPT,
                [
                    'cost' => 12
                ]
            );

            return $user;
        }

        return  null;
    }

    public function retrieveCurrentUserPasswordByUsername(string $username): ?string
    {
        if ($username === self::GOOD_USERNAME) {
            return password_hash(
                'good_password',
                PASSWORD_BCRYPT,
                [
                    'cost' => 12
                ]
            );
        }

        return null;

    }

    public function updateUserInformations(string $username, array $informations): void
    {
        foreach ($this->getUsers() as $index => $user) {
            if ($user['id'] === $username) {
                foreach ($informations as $field => $value) {
                    $user[$field] = $value;
                }
                $this->users[$index] = $user;
                break;
            }
        }
    }

    public function getUsers(): array
    {
        return $this->users;
    }

    public function deleteAccount(string $usernameToDelete): void
    {
        unset($this->users[$usernameToDelete]);
    }
}