<?php

namespace App\FakeImplementation\Security;

use App\Domain\Core\Security\AccessTokenInterface;

class FakeAccessToken implements AccessTokenInterface
{

    public function generate(array $data): string
    {
        return base64_encode(json_encode($data));
    }

    public function decode(string $token): array
    {
         return json_decode(base64_decode($token), true);
    }
}