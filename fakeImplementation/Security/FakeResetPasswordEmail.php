<?php

namespace App\FakeImplementation\Security;

use App\Domain\Core\Security\EmailInterface;
use App\Domain\Core\Security\MailerInterface;

class FakeResetPasswordEmail implements MailerInterface
{
    private array $sentEmails = [];

    public function send(EmailInterface $email): void
    {
        $this->sentEmails[] = [
            'to' => $email->getTo(),
            'subject' => $email->getSubject(),
            'body' => $email->getBody(),
        ];
    }

    public function getSentEmails(): array
    {
        return $this->sentEmails;
    }
}
