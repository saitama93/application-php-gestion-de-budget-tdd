<?php

namespace App\FakeImplementation\Security;

use App\Domain\Core\Security\PasswordInterface;

final class FakePasswordMaker implements PasswordInterface
{
    public function hash(string $plainPassword): string
    {
        return password_hash(
            $plainPassword,
            PASSWORD_BCRYPT,
            [
                'cost' => 12
            ]
        );
    }

    public function verify(string $plainPassword, string $hash): bool
    {
        return password_verify($plainPassword, $hash);
    }
}