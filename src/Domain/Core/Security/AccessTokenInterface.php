<?php

namespace App\Domain\Core\Security;

interface AccessTokenInterface
{
    public function generate(array $data): string;

    public function decode(string $token): array;

}