<?php

namespace App\Domain\Core\Security;

interface EmailInterface
{
    public function getTo(): string;
    public function getSubject(): string;
    public function getBody(): string;
}