<?php

namespace App\Domain\Core\Security;

interface MailerInterface
{
    public function send(EmailInterface $email): void;
}