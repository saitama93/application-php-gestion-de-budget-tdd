<?php

namespace App\Domain\Core\Security;

interface PasswordInterface
{
    public function hash(string $plainPassword): string;

    public function verify(string $plainPassword, string $hash): bool;
}