<?php

namespace App\Domain\Core\Response;

enum ResponseStatusCode: int
{
    case CREATED = 201;
    case BAD_REQUEST = 400;

    case NOT_FOUND = 404;

    case UNAUTHORIZED = 401;

    case NOT_CONTENT = 204;

    case OK = 200;

    public function getValue(): int
    {
        return $this->value;
    }
}