<?php

namespace App\Domain\Core\Response;

abstract class AbstractResponse
{
    private function __construct(
        private bool $isSuccess,
        private int $statusCode,
        private array $data
    ) {}

    public static function from(array $data): self
    {
        return new static(
            isSuccess: $data['isSuccess'] ?? true,
            statusCode: $data['statusCode'] ?? ResponseStatusCode::NOT_CONTENT->getValue(),
            data: $data['data'] ?? []
        );
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
