<?php

namespace App\Domain\User\Repository;

use App\Domain\User\Model\User;

interface UserRepositoryInterface
{
    public function save(User $user): void;

    public function checkIfUsernameAlreadyExists(string $username): bool;

    public function findByUsername(string $username): ?array;

    public function retrieveCurrentUserPasswordByUsername(string $username): ?string;

    public function updateUserInformations(string $username, array $informations): void;

    public function deleteAccount(string $usernameToDelete): void;
}