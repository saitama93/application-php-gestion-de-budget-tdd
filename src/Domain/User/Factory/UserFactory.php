<?php

namespace App\Domain\User\Factory;

use App\Domain\User\Model\User;

final class UserFactory
{
    /**
     * @param array<string, string> $userData
     * @return User
     */
    public static function create(array $userData): User
    {
        return new User(
            firstName: $userData['firstName'],
            lastName: $userData['lastName'],
            username: $userData['username'],
            password: $userData['password'],
        );
    }
}