<?php

namespace App\Domain\User\Model;

use Ramsey\Uuid\Uuid;

final class User
{
    private string $id;

    public function __construct
    (
        private string $firstName,
        private string $lastName,
        private string $username,
        private string $password
    )
    {
        $this->id = Uuid::uuid4()->toString();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }
}