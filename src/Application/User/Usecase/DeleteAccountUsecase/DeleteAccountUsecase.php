<?php

namespace App\Application\User\Usecase\DeleteAccountUsecase;

use App\Application\Core\Exception\PasswordInvalidException;
use App\Application\Core\Exception\UnauthorizedException;
use App\Application\Core\Exception\UsernameNotFoundException;
use App\Domain\Core\Response\AbstractResponse;
use App\Domain\Core\Response\ResponseStatusCode;
use App\Domain\Core\Security\PasswordInterface;
use App\Domain\User\Repository\UserRepositoryInterface;

class DeleteAccountUsecase
{
    public function __construct(
        private UserRepositoryInterface $userRepository,
        private PasswordInterface $password
    )
    {}

    public function execute(DeleteAccountRequest $request): ?AbstractResponse
    {
        if ($request->getUsername() != $request->getUsernameToDelete()){
            throw new UnauthorizedException();
        }

        $userData = $this->userRepository->findByUsername($request->getUsername());

        if (!$userData){
            throw new UsernameNotFoundException($request->getUsername());
        }


        if (!$this->password->verify($request->getPassword(), $userData['password'])) {
            throw new PasswordInvalidException($request->getPassword());
        }

        try {
            $this->userRepository->deleteAccount($request->getUsername());
        } catch (\Exception $exception){}

        return DeleteAccountResponse::from([
            'statusCode' => ResponseStatusCode::NOT_CONTENT->getValue(),
            'isSuccess' => true,
            'data' => []
        ]);
    }
}