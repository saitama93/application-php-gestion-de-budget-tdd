<?php

namespace App\Application\User\Usecase\DeleteAccountUsecase;

use App\Domain\Core\Response\AbstractResponse;

class DeleteAccountResponse extends AbstractResponse
{}