<?php

namespace App\Application\User\Usecase\DeleteAccountUsecase;

class DeleteAccountRequest
{
    private function __construct(
        private string $requesterUsername,
        private string $usernameToDelete,
        private string $password
    )
    {}

    public static function from(array $informations): self
    {
        return new self(
            requesterUsername: $informations['requesterUsername'],
            usernameToDelete: $informations['usernameToDelete'],
            password: $informations['password']
        );
    }

    public function getUsername(): string
    {
        return $this->requesterUsername;
    }

    public function getUsernameToDelete(): string
    {
        return $this->usernameToDelete;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}