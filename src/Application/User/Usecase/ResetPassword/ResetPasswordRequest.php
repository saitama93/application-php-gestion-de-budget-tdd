<?php

namespace App\Application\User\Usecase\ResetPassword;

final class ResetPasswordRequest
{
    private function __construct(
        public string $username,
        public string $currentPlainPassword,
        public string $newPlainPassword,
    ) {}

    public static function from(string $username, string $currentPlainPassword, string $newPlainPassword): self
    {
        return new self(
            username: $username,
            currentPlainPassword: $currentPlainPassword,
            newPlainPassword: $newPlainPassword
        );
    }
}