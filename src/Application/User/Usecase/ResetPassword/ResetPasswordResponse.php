<?php

namespace App\Application\User\Usecase\ResetPassword;

use App\Domain\Core\Response\AbstractResponse;

final class ResetPasswordResponse extends AbstractResponse
{}
