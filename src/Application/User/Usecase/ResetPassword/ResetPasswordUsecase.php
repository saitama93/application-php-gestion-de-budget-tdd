<?php

namespace App\Application\User\Usecase\ResetPassword;


use App\Application\Core\Exception\PasswordInvalidException;
use App\Application\Core\Exception\UserNotFoundException;
use App\Domain\Core\Response\AbstractResponse;
use App\Domain\Core\Response\ResponseStatusCode;
use App\Domain\Core\Security\PasswordInterface;
use App\Domain\User\Repository\UserRepositoryInterface;

class ResetPasswordUsecase
{
    public function __construct(
        private UserRepositoryInterface $userRepository,
        private PasswordInterface $password
    )
    {}

    public function execute(ResetPasswordRequest $request): AbstractResponse
    {
        $currentHashedUserPassword = $this->userRepository->retrieveCurrentUserPasswordByUsername($request->username);

        if (!$currentHashedUserPassword) {
            throw new UserNotFoundException($request->username);
        }

        // comparer le current password avec le $currentHashedUserPassword
        if (!$this->password->verify($request->currentPlainPassword, $currentHashedUserPassword)) {
            throw new PasswordInvalidException($request->currentPlainPassword);
        }

        try {
            $this->userRepository->updateUserInformations($request->username, [
                'password' => $this->password->hash($request->currentPlainPassword)
            ]);
        } catch (\Exception $exception) {}

        return ResetPasswordResponse::from([
            'statusCode' => ResponseStatusCode::NOT_CONTENT->getValue(),
            'isSuccess' => true,
            'data' => []
        ]);
    }
}