<?php

namespace App\Application\Core\Exception;

use App\Domain\Core\Response\ResponseStatusCode;
use RuntimeException;

class UserNotFoundException extends RuntimeException
{
    public function __construct(string $userId)
    {
        parent::__construct(
            sprintf("User with id [%s] does not exist.", $userId),
            ResponseStatusCode::NOT_FOUND->getValue()
        );
    }
}