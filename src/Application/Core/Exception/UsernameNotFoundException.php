<?php

namespace App\Application\Core\Exception;

use App\Domain\Core\Response\ResponseStatusCode;
use RuntimeException;

final class UsernameNotFoundException extends RuntimeException
{
    public function __construct(string $email)
    {
        parent::__construct(
            sprintf("Username %s not exist.", $email),
            ResponseStatusCode::NOT_FOUND->getValue()
        );
    }
}
