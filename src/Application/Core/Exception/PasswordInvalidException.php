<?php

namespace App\Application\Core\Exception;

use App\Domain\Core\Response\ResponseStatusCode;
use RuntimeException;

class PasswordInvalidException extends RuntimeException
{
    public function __construct(string $currentPassword)
    {
        parent::__construct(
            sprintf("Current password [%s] is invalid.", $currentPassword),
            ResponseStatusCode::UNAUTHORIZED->getValue()
        );
    }
}