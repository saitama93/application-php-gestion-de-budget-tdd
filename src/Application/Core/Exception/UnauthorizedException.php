<?php

namespace App\Application\Core\Exception;

use App\Domain\Core\Response\ResponseStatusCode;
use RuntimeException;

class UnauthorizedException extends RuntimeException
{
    public function __construct()
    {
        parent::__construct(
            'Access denied.',
            ResponseStatusCode::UNAUTHORIZED->getValue()
        );
    }
}