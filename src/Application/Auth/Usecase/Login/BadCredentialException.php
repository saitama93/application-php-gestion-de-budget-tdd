<?php

namespace App\Application\Auth\Usecase\Login;

use App\Domain\Core\Response\ResponseStatusCode;
use RuntimeException;

final class BadCredentialException extends RuntimeException
{
    public function __construct()
    {
        parent::__construct('Bad credentials.', ResponseStatusCode::UNAUTHORIZED->getValue());
    }
}
