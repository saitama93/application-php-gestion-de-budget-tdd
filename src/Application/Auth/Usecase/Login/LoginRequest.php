<?php

namespace App\Application\Auth\Usecase\Login;

final class LoginRequest
{
    private function __construct(
        private string $username,
        private string $password
    )
    {}

    public static function from(array $credentials): self
    {
        return new self(
            username: $credentials['username'],
            password: $credentials['password']
        );
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}