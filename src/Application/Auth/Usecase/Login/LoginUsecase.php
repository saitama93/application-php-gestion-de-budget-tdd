<?php

namespace App\Application\Auth\Usecase\Login;

use App\Application\Core\Exception\UsernameNotFoundException;
use App\Domain\Core\Response\AbstractResponse;
use App\Domain\Core\Response\ResponseStatusCode;
use App\Domain\Core\Security\AccessTokenInterface;
use App\Domain\Core\Security\PasswordInterface;
use App\Domain\User\Repository\UserRepositoryInterface;

final class LoginUsecase
{
    public function __construct(
        private UserRepositoryInterface $userRepository,
        private PasswordInterface $password,
        private AccessTokenInterface $accessToken
    )
    {}

    public function execute(LoginRequest $request): AbstractResponse
    {
        $userData = $this->userRepository->findByUsername($request->getUsername());
        if (!$userData){
            throw new UsernameNotFoundException($request->getUsername());
        }

        if (!$this->password->verify($request->getPassword(), $userData['password'])){
            throw new BadCredentialException();
        }

        return LoginResponse::from([
            'data' => [
                'token' => $this->accessToken->generate([
                    'id' => $userData['id']
                ])
            ],
            'statusCode' => ResponseStatusCode::OK->getValue()
        ]);
    }
}