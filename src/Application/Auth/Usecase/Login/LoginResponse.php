<?php

namespace App\Application\Auth\Usecase\Login;

use App\Domain\Core\Response\AbstractResponse;

final class LoginResponse extends AbstractResponse
{}
