<?php

namespace App\Application\Auth\Usecase\Registration;

final class RegistrationRequest
{
    private function __construct(
        private string $firstname,
        private string $lastname,
        private string $username,
        private string $password,
    ) {}

    public static function from(array $userData): self
    {
        return new self(
            firstname: $userData['firstname'],
            lastname: $userData['lastname'],
            username: $userData['username'],
            password: $userData['password'],
        );
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}