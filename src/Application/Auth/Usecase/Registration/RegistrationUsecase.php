<?php

namespace App\Application\Auth\Usecase\Registration;

use App\Domain\Core\Response\AbstractResponse;
use App\Domain\Core\Response\ResponseStatusCode;
use App\Domain\Core\Security\PasswordInterface;
use App\Domain\User\Factory\UserFactory;
use App\Domain\User\Repository\UserRepositoryInterface;

final class RegistrationUsecase
{
    public function __construct(
        private PasswordInterface $password,
        private UserRepositoryInterface $repository
    ) {}

    public function execute(RegistrationRequest $request): AbstractResponse
    {
        if ($this->repository->checkIfUsernameAlreadyExists($request->getUsername())) {
            throw new UserAlreadyExistsException($request->getUsername());
        }

        $user = UserFactory::create([
            'firstName' => $request->getFirstname(),
            'lastName' => $request->getLastname(),
            'username' => $request->getUsername(),
            'password' => $this->password->hash($request->getPassword()),
        ]);

        try {
            $this->repository->save($user);
        } catch (\Exception $exception) {}

        return RegistrationResponse::from([
            'statusCode' => ResponseStatusCode::CREATED->getValue(),
            'data' => [
                'user_id' => $user->getId()
            ]
        ]);
    }
}