<?php

namespace App\Application\Auth\Usecase\Registration;

use App\Domain\Core\Response\ResponseStatusCode;
use RuntimeException;

final class UserAlreadyExistsException extends RuntimeException
{
    public function __construct(string $username)
    {
        parent::__construct(
            sprintf("Username [%s] already exists.", $username),
            ResponseStatusCode::BAD_REQUEST->getValue()
        );
    }
}
