<?php

namespace App\Application\Auth\Usecase\Registration;

use App\Domain\Core\Response\AbstractResponse;

final class RegistrationResponse extends AbstractResponse
{}
