<?php

namespace App\Tests\Application\User\ResetPassword;

use App\Application\Core\Exception\PasswordInvalidException;
use App\Application\Core\Exception\UserNotFoundException;
use App\Application\User\Usecase\ResetPassword\ResetPasswordRequest;
use App\Application\User\Usecase\ResetPassword\ResetPasswordResponse;
use App\Application\User\Usecase\ResetPassword\ResetPasswordUsecase;
use App\Domain\Core\Response\ResponseStatusCode;
use App\FakeImplementation\Repository\FakeUserRepository;
use App\FakeImplementation\Security\FakePasswordMaker;
use PHPUnit\Framework\TestCase;

final class ResetPasswordTest extends TestCase
{
    private ResetPasswordUsecase $resetPassword;
    protected function setUp(): void
    {
        $userRepository = new FakeUserRepository();
        $password = new FakePasswordMaker();
        $this->resetPassword = new ResetPasswordUsecase($userRepository, $password);
    }

    /** @test */
    public function can_not_reset_password_with_not_exist_account():void
    {
        $this->expectException(UserNotFoundException::class);
        $this->expectExceptionCode(ResponseStatusCode::NOT_FOUND->getValue());
        $this->expectExceptionMessage('User with id [userid] does not exist.');

        $this->resetPassword->execute(ResetPasswordRequest::from(
            'userid',
            'password',
            'newPassword'
        ));
    }

    /** @test */
    public function can_not_reset_password_with_invalid_password():void
    {
        $this->expectException(PasswordInvalidException::class);
        $this->expectExceptionCode(ResponseStatusCode::UNAUTHORIZED->getValue());
        $this->expectExceptionMessage('Current password [passwordd] is invalid.');

        $this->resetPassword->execute(ResetPasswordRequest::from(
            FakeUserRepository::GOOD_USERNAME,
            'passwordd',
            'newPassword'
        ));
    }

    /** @test */
    public function can_reset_password_with_valid_password():void
    {
        $response = $this->resetPassword->execute(ResetPasswordRequest::from(
            FakeUserRepository::GOOD_USERNAME,
            'good_password',
            'newPassword'
        ));

        $this->assertInstanceOf(ResetPasswordResponse::class, $response);
        $this->assertEquals(ResponseStatusCode::NOT_CONTENT->getValue(), $response->getStatusCode());
    }
}