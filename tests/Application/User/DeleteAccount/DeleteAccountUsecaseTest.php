<?php

namespace App\Tests\Application\User\DeleteAccount;

use App\Application\Core\Exception\PasswordInvalidException;
use App\Application\Core\Exception\UnauthorizedException;
use App\Application\User\Usecase\DeleteAccountUsecase\DeleteAccountRequest;
use App\Application\User\Usecase\DeleteAccountUsecase\DeleteAccountResponse;
use App\Application\User\Usecase\DeleteAccountUsecase\DeleteAccountUsecase;
use App\Domain\Core\Response\ResponseStatusCode;
use App\FakeImplementation\Repository\FakeUserRepository;
use App\FakeImplementation\Security\FakePasswordMaker;
use PHPUnit\Framework\TestCase;

class DeleteAccountUsecaseTest extends TestCase
{
    private DeleteAccountUsecase $delete;

    protected function setUp(): void
    {
        $userRepository = new FakeUserRepository();
        $password = new FakePasswordMaker();
        $this->delete = new DeleteAccountUsecase($userRepository, $password);
    }

    /** @test */
    public function can_not_delete_no_existing_account()
    {
        $this->expectException(UnauthorizedException::class);
        $this->expectExceptionCode(ResponseStatusCode::UNAUTHORIZED->getValue());
        $this->expectExceptionMessage('Access denied.');

        $this->delete->execute(DeleteAccountRequest::from([
            'requesterUsername' => 'badUsername',
            'usernameToDelete' => 'username',
            'password' => 'bad_password'
        ]));
    }

    /** @test */
    public function can_not_delete_of_other_user()
    {
        $this->expectException(UnauthorizedException::class);
        $this->expectExceptionCode(ResponseStatusCode::UNAUTHORIZED->getValue());
        $this->expectExceptionMessage('Access denied.');

        $this->delete->execute(DeleteAccountRequest::from([
            'requesterUsername' => FakeUserRepository::GOOD_USERNAME,
            'usernameToDelete' => 'username',
            'password' => 'bad_password'
        ]));
    }

    /** @test */
    public function can_not_delete_with_good_username_and_bad_password()
    {
        $this->expectException(PasswordInvalidException::class);
        $this->expectExceptionCode(ResponseStatusCode::UNAUTHORIZED->getValue());
        $this->expectExceptionMessage('Current password [bad_password] is invalid.');

        $this->delete->execute(DeleteAccountRequest::from([
            'requesterUsername' => FakeUserRepository::GOOD_USERNAME,
            'usernameToDelete' => FakeUserRepository::GOOD_USERNAME,
            'password' => 'bad_password'
        ]));
    }

    /** @test */
    public function can_delete_account()
    {
        $response = $this->delete->execute(DeleteAccountRequest::from([
            'requesterUsername' => FakeUserRepository::GOOD_USERNAME,
            'usernameToDelete' => FakeUserRepository::GOOD_USERNAME,
            'password' => 'password'
        ]));


        $this->assertInstanceOf(DeleteAccountResponse::class, $response);
        $this->assertEquals(ResponseStatusCode::NOT_CONTENT->getValue(), $response->getStatusCode());
    }
}