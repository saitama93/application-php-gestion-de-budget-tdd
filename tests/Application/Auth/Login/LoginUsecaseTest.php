<?php

namespace App\Tests\Application\Auth\Login;

use App\Application\Auth\Usecase\Login\BadCredentialException;
use App\Application\Auth\Usecase\Login\LoginRequest;
use App\Application\Auth\Usecase\Login\LoginUsecase;
use App\Application\Core\Exception\UsernameNotFoundException;
use App\Domain\Core\Response\ResponseStatusCode;
use App\FakeImplementation\Repository\FakeUserRepository;
use App\FakeImplementation\Security\FakeAccessToken;
use App\FakeImplementation\Security\FakePasswordMaker;
use PHPUnit\Framework\TestCase;

class LoginUsecaseTest extends TestCase
{
    private LoginUsecase $login;
    private FakeAccessToken $fakeAccessToken;

    protected function setUp(): void
    {
        $userRepository = new FakeUserRepository();
        $fakePasswordMaker = new FakePasswordMaker();
        $this->fakeAccessToken = new FakeAccessToken();
        $this->login = new LoginUsecase($userRepository, $fakePasswordMaker, $this->fakeAccessToken);
    }

    /** @test */
    public function can_not_login_user_with_not_exist_email()
    {
        $this->expectException(UsernameNotFoundException::class);
        $this->expectExceptionCode(ResponseStatusCode::NOT_FOUND->getValue());
        $this->expectExceptionMessage('Username badUsername not exist');

       $this->login->execute(LoginRequest::from(
            [
                'username' => 'badUsername',
                'password' => 'password'
            ]
       ));
    }

    /** @test */
    public function can_not_login_user_with_bad_password(): void
    {
        $this->expectException(BadCredentialException::class);
        $this->expectExceptionCode(ResponseStatusCode::UNAUTHORIZED->getValue());
        $this->expectExceptionMessage('Bad credentials');

        $this->login->execute(LoginRequest::from(
            [
                'username' => 'user1',
                'password' => 'bad_password'
            ]
        ));
    }

    /** @test */
    public function can_login_user(): void
    {
        $response = $this->login->execute(LoginRequest::from(
            [
                'username' => 'user1',
                'password' => 'password'
            ]
        ));

        $token = $response->getData()['token'];
        $this->assertNotNull($token);
        $this->assertSame(ResponseStatusCode::OK->getValue(), $response->getStatusCode());
        $this->assertTrue($response->isSuccess());
        $this->assertArrayHasKey('id', $this->fakeAccessToken->decode($token));
    }
}