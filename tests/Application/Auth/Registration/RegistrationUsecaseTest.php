<?php

namespace App\Tests\Application\Auth\Registration;

use App\Application\Auth\Usecase\Registration\RegistrationRequest;
use App\Application\Auth\Usecase\Registration\RegistrationUsecase;
use App\Application\Auth\Usecase\Registration\UserAlreadyExistsException;
use App\Domain\Core\Response\ResponseStatusCode;
use App\FakeImplementation\Repository\FakeUserRepository;
use App\FakeImplementation\Security\FakePasswordMaker;
use PHPUnit\Framework\TestCase;

final class RegistrationUsecaseTest extends TestCase
{
    private RegistrationUsecase $registration;
    private FakeUserRepository $fakeUserRepository;

    protected function setUp(): void
    {
        $fakePasswordMaker = new FakePasswordMaker();
        $this->fakeUserRepository = new FakeUserRepository();
        $this->registration = new RegistrationUsecase(
            $fakePasswordMaker,
            $this->fakeUserRepository
        );
    }

    /** @test */
    public function can_create_user_successfully()
    {
        $response = $this->registration->execute($this->getRequest());

        $this->assertTrue($response->isSuccess());
        $this->assertSame(ResponseStatusCode::CREATED->getValue(), $response->getStatusCode());
        $this->assertNotNull($response->getData()['user_id']);
        $this->assertCount(3, $this->fakeUserRepository->getUsers());
    }

    /** @test */
    public function can_not_create_user_with_same_username_successfully()
    {
        $this->expectException(UserAlreadyExistsException::class);
        $this->expectExceptionCode(ResponseStatusCode::BAD_REQUEST->getValue());
        $this->expectExceptionMessage('Username [john_doe] already exists.');
        $this->registration->execute($this->getRequest());
        $this->registration->execute($this->getRequest());
    }

    private function getRequest(): RegistrationRequest
    {
        return RegistrationRequest::from([
            'firstname' => 'John',
            'lastname'  => 'Doe',
            'username'  => 'john_doe',
            'password'  => 'password',
        ]);
    }
}