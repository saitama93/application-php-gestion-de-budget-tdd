# Reset password Usecase

$resetPassword = new ResetPasswordUsecase();
$response = $resetPassword->execute(ResetPasswordRequest $resetPassword): ResetPasswordResponse;

Si le compte n'existe pas, on ne reset pas le password => EmailNotFoundException
Si le mail existe on renouvelle le mot de passe en le hashant