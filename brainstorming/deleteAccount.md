# Delete Account Usecase

$delete = new DeleteAccountUsecase();
$response = $delete->execute(DeleteAccountRequest): DeleteAccountResponse;

Si le compte n'existe pas, on ne le supprime pas => EmailNotFoundException
Si le compte existe, on vérifie si la personne qui fait la demande est le propriétaire du comtpe
    - Si ce n'est pas le propriétaire alors on ne supprime pas le comtpe
    - Si c'est le propriétaire alors on supprime le compte