# Login Usecase

$login = new LoginUsecase();
$response = login->execute(LoginRequest $request): LoginResponse;

Si email n'existe pas, throw exception => EmailNotFoundException
Si email existe mais mauvais mot de passe, throw exception => BadCredentialException
LoginResponse si bon credential return => accessToken

[
    'identifiant' => 'id',
]
